import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogicMiddleware } from 'redux-logic';
import reducerArray from './reducers';
import logicArray from './logic';

const rootReducer = combineReducers(reducerArray);
const logicMiddleware = createLogicMiddleware(logicArray);
const enhancer = composeWithDevTools(applyMiddleware(logicMiddleware));

export default createStore(
  rootReducer,
  enhancer
);
