import actionCreator from '../util/actionCreator';

export const dialogMessages = {
  OPEN: 'dialog/OPEN',
  CLOSE: 'dialog/CLOSE'
};

export const dialogActions = {
  opendialog: actionCreator(dialogMessages.OPEN, 'data'),
  closeDialog: actionCreator(dialogMessages.CLOSE)
};

export const dialogSelectors = {
  isOpen: (state) => Object.keys(state.dialog).length > 0,
  getDialog: (state) => state.dialog || false
};
