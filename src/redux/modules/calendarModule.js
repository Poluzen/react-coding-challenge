import actionCreator from '../util/actionCreator';

export const calendarMessages = {
  CHANGE_MONTH: 'calendar/CHANGE_MONTH',
  SELECT_DAY: 'calendar/SELECT_DAY'
};

export const calendarActions = {
  changeMonth: actionCreator(calendarMessages.CHANGE_MONTH, 'direction'),
  selectDate: actionCreator(calendarMessages.SELECT_DAY, 'selectedDate')
};

export const calendarSelectors = {
  getCurrentDate: (state) => state.calendar.currentDate,
  getActiveDate: (state) => state.calendar.activeDate,
  getMonthName: (state) => state.calendar.activeMonthName,
  getFullYear: (state) => state.calendar.activeFullYear,
  getStartDate: (state) => state.calendar.startDate,
  getSelectedDate: (state) => state.calendar.selectedDate,
  getDays: (state) => state.calendar.days
};
