import actionCreator from '../util/actionCreator';
import { equalDate } from '../../util/helper';

export const reminderMessages = {
  SAVE: 'reminder/SAVE',
  REMOVE: 'reminder/REMOVE'
};

export const reminderActions = {
  saveReminder: actionCreator(reminderMessages.SAVE, 'data'),
  removeReminder: actionCreator(reminderMessages.REMOVE, 'id')
};

export const reminderSelectors = {
  getAllReminders: (state) => state.reminders,
  sortRemindersForDate: (reminders, date) => (
    reminders
      .filter((r) => equalDate(r.date, date))
      .sort((a, b) => new Date(a.date) - new Date(b.date))
  )
};
