import { createLogic } from 'redux-logic';
import { calendarMessages, calendarSelectors } from '../modules/calendarModule';
import { changeMonth, getMonthName, getCalendarStartDate, getCalendarDays } from '../../util/helper';

const createCalendarData = createLogic({
  type: calendarMessages.CHANGE_MONTH,
  transform({ getState, action }, next) {
    const { type, direction } = action;
    const lastActiveMonth = calendarSelectors.getActiveDate(getState());

    const currentDate = new Date();
    const activeDate = changeMonth(lastActiveMonth, direction);
    const startDate = getCalendarStartDate(activeDate);

    next({
      type,
      currentDate,
      activeDate,
      activeMonthName: getMonthName(activeDate),
      activeFullYear: activeDate.getFullYear(),
      startDate,
      days: getCalendarDays(currentDate, activeDate, startDate)
    });
  }
});

export default [
  createCalendarData
];
