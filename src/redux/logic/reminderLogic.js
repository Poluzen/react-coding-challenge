import shortid from 'shortid';
import {createLogic } from 'redux-logic';
import { reminderMessages, reminderSelectors } from '../modules/reminderModule';

const saveReminder = createLogic({
  type: reminderMessages.SAVE,
  transform({ getState, action }, next) {
    const state = getState();
    const reminders = reminderSelectors.getAllReminders(state);
    const {type, data} = action;

    const index = reminders.findIndex((reminder) => reminder.id === data.id);
    if (~index) {
      reminders.splice(index, 1);
    }

    const {date, time} = data;
    const [hours, minutes] = time.split(':');

    const newDate = new Date(date);
    newDate.setHours(parseInt(hours), parseInt(minutes));

    next({
      type,
      ...data,
      date: newDate,
      id: shortid.generate()
    });
  }
});

const removeReminder = createLogic({
  type: reminderMessages.REMOVE,
  transform({getState, action}, next) {
    const state = getState();
    const reminders = reminderSelectors.getAllReminders(state);

    const index = reminders.findIndex((reminder) => reminder.id === action.id);
    if (~index) {
      reminders.splice(index, 1);
    }

    next({
      type: action.type,
      reminders
    });
  }
});

export default [
  saveReminder,
  removeReminder
];
