import calendarLogic from './calendarLogic';
import reminderLogic from './reminderLogic';

export default [
  ...calendarLogic,
  ...reminderLogic
];
