import { reminderMessages } from '../modules/reminderModule';

const reminderReducer = (state = [], action) => {
  switch (action.type) {
    case reminderMessages.SAVE:
      return [
        ...state,
        {
          id: action.id,
          date: action.date,
          text: action.text,
          color: action.color,
          time: action.time
        }
      ];
    case reminderMessages.REMOVE:
      return [
        ...action.reminders
      ];
    default:
      return state;
  }
};

export default reminderReducer;
