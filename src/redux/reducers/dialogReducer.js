import { dialogMessages } from '../modules/dialogModule';

const dialogReducer = (state = {}, action) => {
  switch (action.type) {
    case dialogMessages.OPEN:
      return action.data;
    case dialogMessages.CLOSE:
      return {};
    default:
      return state;
  }
};

export default dialogReducer;
