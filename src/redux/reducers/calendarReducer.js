import { calendarMessages } from '../modules/calendarModule';
import { getFirstDayOfMonth, getCalendarStartDate, getMonthName, getCalendarDays } from '../../util/helper';

const currentDate = new Date();
const activeDate = getFirstDayOfMonth(new Date());
const startDate = getCalendarStartDate(activeDate);
const initialState = {
  currentDate,
  activeDate,
  activeMonthName: getMonthName(activeDate),
  activeFullYear: activeDate.getFullYear(),
  startDate,
  selectedDate: null,
  days: getCalendarDays(currentDate, activeDate, startDate)
};

const calendarReducer = (state = initialState, action) => {
  switch (action.type) {
    case calendarMessages.CHANGE_MONTH:
      return {
        ...state,
        currentDate: new Date(),
        activeDate: action.activeDate,
        activeMonthName: action.activeMonthName,
        activeFullYear: action.activeFullYear,
        startDate: action.startDate,
        selectedDate: null,
        days: action.days
      };
    case calendarMessages.SELECT_DAY:
      return {
        ...state,
        selectedDate: action.selectedDate
      };
    default:
      return state;
  }
};

export default calendarReducer;
