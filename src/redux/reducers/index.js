import calendarReducer from './calendarReducer';
import reminderReducer from './reminderReducer';
import dialogReducer from './dialogReducer';

export default {
  calendar: calendarReducer,
  reminders: reminderReducer,
  dialog: dialogReducer
};
