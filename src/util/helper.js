import shortid from 'shortid';

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const dayNames = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

const TOTAL_DAYS_SHOWN = 6 * 7;
const SECONDS_PER_DAY = 86400;
const MILLISECONDS_PER_DAY = SECONDS_PER_DAY * 1000;

function toLocaleStringSupportsLocales() {
  try {
    new Date().toLocaleString('i');
  } catch (e) {
    return e instanceof RangeError;
  }
  return false;
}

const supportsLocaleString = toLocaleStringSupportsLocales();

const getMonthName = (date, locale = 'en-gb', options = { month: 'long' }) => {
  if (supportsLocaleString) {
    return date.toLocaleString(locale, options);
  } else {
    return monthNames[date.getMonth()];
  }
};

const getFirstDayOfMonth = (date) => {
  return new Date(date.getFullYear(), date.getMonth(), 1);
};

const getWeekDaysList = () => {
  return Array.from(dayNames).map((name) => name.substr(0, 3));
};

const getCalendarStartDate = (date) => {
  const monthStartDay = date.getDay();
  return new Date(date.getTime() - (monthStartDay * MILLISECONDS_PER_DAY));
};

const getCalendarDays = (currentDate, activeDate, startDate) => {
  return Array.apply(null, { length: TOTAL_DAYS_SHOWN }).map((value, index) => {
    const date = new Date(startDate.getTime() + (index * MILLISECONDS_PER_DAY))
    return {
      key: shortid.generate(),
      date,
      disabled: !equalMonth(date, activeDate),
      today: equalDate(date, currentDate)
    };
  });
};

const equalYear = (a, b) => {
  return a.getFullYear() === b.getFullYear();
};

const equalMonth = (a, b) => {
  return a.getMonth() === b.getMonth()
};

const equalDate = (a, b) => {
  return (
    equalYear(a, b) &&
    equalMonth(a, b) &&
    a.getDate() === b.getDate()
  );
};

const changeMonth = (date, direction) => {
  return getFirstDayOfMonth(new Date(date.setMonth(date.getMonth() + direction)));
};

export {
  getMonthName,
  getFirstDayOfMonth,
  getWeekDaysList,
  getCalendarStartDate,
  getCalendarDays,
  equalYear,
  equalMonth,
  equalDate,
  changeMonth
};
