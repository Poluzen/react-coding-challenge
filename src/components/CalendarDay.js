import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { calendarActions, calendarSelectors } from '../redux/modules/calendarModule';
import { dialogActions } from '../redux/modules/dialogModule';
import { equalDate } from '../util/helper';
import Reminder from './Reminder';

const CalendarDay = ({ openDialog, selectDate, ...props}) => {
  const {date, selectedDate, key, disabled, today, reminders} = props;
  const selected = selectedDate && equalDate(date, selectedDate);
  const listProps = {
    key,
    className: classNames({
      'disabled': disabled,
      'today': today,
      'selected': selected,
    }),
    onClick: disabled || selected ? undefined : () => selectDate(date)
  };

  const disabledRemindersText = reminders.length > 0 ? `${reminders.length} hidden reminders` : '';

  return (
    <li {...listProps}>
      <div className="day-header">
        <span className="date">{date.getDate()}</span>
        <span className="add" onClick={() => openDialog({ date })}>&#65291;</span>
      </div>
      <div className="day-reminders">
        {
          disabled
            ? <span>{disabledRemindersText}</span>
            : reminders.map((reminder) => <Reminder key={reminder.id} {...reminder} />)
        }
      </div>
    </li>
  );
};

const mapStateToProps = (state) => ({
  selectedDate: calendarSelectors.getSelectedDate(state)
});

const mapToDispatch = (dispatch) => ({
  selectDate: (date) => dispatch(calendarActions.selectDate(date)),
  openDialog: (data) => dispatch(dialogActions.opendialog(data))
});

export default connect(mapStateToProps, mapToDispatch)(CalendarDay);
