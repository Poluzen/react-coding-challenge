import React from 'react';
import { connect } from 'react-redux';
import { dialogActions } from '../redux/modules/dialogModule';
import { reminderActions } from '../redux/modules/reminderModule';

const Reminder = ({ openDialog, removeReminder, ...props }) => (
  <div className="reminder">
    <span className="reminder-color" style={{ backgroundColor: props.color }} />
    <div className="reminder-text">
      <div>
        <span className="reminder-time">{props.time}</span>
        <span className="reminder-btn reminder-edit" onClick={() => openDialog(props)}>&#9998;</span>
        <span className="reminder-btn reminder-delete" onClick={() => removeReminder(props.id)}>&#10005;</span>
      </div>
      <span>{props.text}</span>
    </div>
  </div>
);

const mapToDispatch = (dispatch) => ({
  openDialog: (data) => dispatch(dialogActions.opendialog(data)),
  removeReminder: (id) => dispatch(reminderActions.removeReminder(id))
});

export default connect(null, mapToDispatch)(Reminder);
