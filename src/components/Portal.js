import { Component } from 'react';
import { createPortal } from 'react-dom';

class Portal extends Component {
  constructor(props) {
    super(props);

    this.target = this.props.target || document.body;
    this.container = document.querySelector(`.${this.props.className}`);

    if (!this.container) {
      this.container = document.createElement('div');
      this.container.classList.add(this.props.className);
    }
  }

  componentDidMount() {
    this.target.appendChild(this.container);
  }

  componentWillUnmount() {
    this.target.removeChild(this.container);
  }

  render() {
    return createPortal(this.props.children, this.container);
  }
}

export default Portal;
