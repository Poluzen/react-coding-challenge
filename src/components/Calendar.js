import React from 'react';
import { connect } from 'react-redux';
import { calendarActions, calendarSelectors } from '../redux/modules/calendarModule';
import { reminderSelectors } from '../redux/modules/reminderModule';
import { getWeekDaysList } from '../util/helper';
import CalendarDay from './CalendarDay';
import './Calendar.css';

const MonthHeader = ({ changeMonth, ...props }) => (
  <div className="calender-header">
    <ul>
      <li className="btn" onClick={() => changeMonth(-1)}>&#10094;</li>
      <li className="title">{props.monthName}<br /><span>{props.fullYear}</span></li>
      <li className="btn" onClick={() => changeMonth(1)}>&#10095;</li>
    </ul>
  </div>
);

const WeekDaysList = () => (
  <ul className="weekday-header">
    {getWeekDaysList().map((name) => <li key={name}>{name}</li>)}
  </ul>
);

const DaysList = ({ sortRemindersForDate, ...props }) => (
  <ul className="days">
    {
      props.days.map((day) => {
        const reminders = sortRemindersForDate(props.reminders, day.date)
        return <CalendarDay {...day} reminders={reminders} />
      })
    }
  </ul>
);

const Calendar = ({ changeMonth, sortRemindersForDate, ...props }) => (
  <div className="calendar">
    <MonthHeader
      monthName={props.monthName}
      fullYear={props.fullYear}
      changeMonth={changeMonth}
    />
    <WeekDaysList />
    <DaysList
      days={props.days}
      reminders={props.reminders}
      sortRemindersForDate={sortRemindersForDate}
    />
  </div>
);

const mapStateToProps = (state) => ({
  monthName: calendarSelectors.getMonthName(state),
  fullYear: calendarSelectors.getFullYear(state),
  days: calendarSelectors.getDays(state),
  reminders: reminderSelectors.getAllReminders(state),
  sortRemindersForDate: (reminders, date) => reminderSelectors.sortRemindersForDate(reminders, date)
});

const mapToDispatch = (dispatch) => ({
  changeMonth: (direction) => dispatch(calendarActions.changeMonth(direction))
});

export default connect(mapStateToProps, mapToDispatch)(Calendar);
