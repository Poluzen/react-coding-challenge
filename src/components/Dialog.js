import React, { Component } from 'react';
import { connect } from 'react-redux';
import { dialogSelectors, dialogActions } from '../redux/modules/dialogModule';
import { reminderActions } from '../redux/modules/reminderModule';
import Portal from './Portal';
import './Dialog.css';

const validateData = ({ text, time, color }) => {
  return (
    text && text.length <= 25 &&
    time && time.length === 5 &&
    color && color.length === 7
  );
};

class Dialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      time: '08:00',
      text: '',
      color: '#53A385'
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isOpen && nextProps.isOpen) {
      const { time, text, color } = nextProps.data;
      this.setState({
        time: time || this.state.time,
        text: text || '',
        color: color || this.state.color
      })
    }
  }

  onChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  saveAndClose = () => {
    if (validateData(this.state)) {
      const { data, saveReminder, closeDialog } = this.props;
      saveReminder({ ...data, ...this.state });
      closeDialog();
    } else {
      this.setState()
    }
  }

  render() {
    const { isOpen, closeDialog } = this.props;
    if (isOpen) {
      return (
        <Portal target={document.querySelector('#root')} className="dialog-container">
          <div className="dialog">
            <span className="dialog-header">Create a reminder</span>
            <form className="dialog-form">
              <input type="time" value={this.state.time} onChange={(e) => this.onChange(e, 'time')} required />
              <input type="text" value={this.state.text} onChange={(e) => this.onChange(e, 'text')} maxLength="25" required />
              <input type="color" value={this.state.color} onChange={(e) => this.onChange(e, 'color')} required />
            </form>
            <div className="dialog-footer">
              <div className="dialog-btn btn-save"onClick={this.saveAndClose}>Save</div>
              <div className="dialog-btn btn-cancel"onClick={closeDialog}>Cancel</div>
            </div>
          </div>
        </Portal>
      );
    }
    return false;
  }
}

const mapStateToProps = (state) => ({
  isOpen: dialogSelectors.isOpen(state),
  data: dialogSelectors.getDialog(state)
});

const mapToDispatch = (dispatch) => ({
  saveReminder: (data) => dispatch(reminderActions.saveReminder(data)),
  deleteReminder: (id) => dispatch(reminderActions.removeReminder(id)),
  closeDialog: () => dispatch(dialogActions.closeDialog())
});

export default connect(mapStateToProps, mapToDispatch)(Dialog);
