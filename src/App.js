import React from 'react';
import Calendar from './components/Calendar';
import Dialog from './components/Dialog';

const App = () => (
  <React.Fragment>
    <Calendar />
    <Dialog />
  </React.Fragment>
);

export default App;
